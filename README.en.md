# xbr_gaode_location

#### 基础包
高德 定位 包：xbr_gaode_location：定位保活
高德 地图 包：xbr_gaode_map
高德 搜索 包：xbr_gaode_search

####复合集成包
高德导航包（已包含定位+地图+搜索包），不要重复引入：xbr_gaode_navi_map
(遗弃)高德猎鹰轨迹包（单包，不可再添加 xbr_gaode_location）：xbr_gaode_track 不建议使用，BUG太多，无法后台定位
(遗弃)高德猎鹰轨迹+定位包：xbr_gaode_location_and_track 不建议使用，BUG太多，无法后台定位

* 采集轨迹建议走WEB服务建立终端，使用xbr_gaode_location定时采集定时上传后台，xbr_gaode_location后台定位设置backgroundService为true后会自动建立双服务保活，息屏也能定位，采集后从后台提交到猎鹰终端

#### 推荐使用合包
 ```dart
   xbr_gaode_navi_amap: ^2.0.5
 ```

#### 单独使用

##### (1).定位权限配置，使用第三方 permission_handler 动态权限工具，  使用方法请移步 permission_handler
##### (2).定位调用

   ```dart
       //TODO:开始单次定位 定位成功自动销毁
       XbrLocation.instance().execOnceLocation(callback: (LocationInfo? location){
           //location 对IOS和Android数据 已兼容处理
   
       });
   
       //TODO:开启连续定位
       XbrLocation.instance().startTimeLocation(
           clientKey: "clientKey",//必传，整个APP只有一个定位线程时可以不传
           callback: (LocationInfo? location){
           	//location 对IOS和Android数据 已兼容处理
       	},
       );
   
       @override
       void dispose() {
           //销毁 连续定位
           XbrLocation.instance().destroyLocation(clientKey);
           super.dispose();
       }
   ```
##### (3).注意：单次定位不用销毁也可以不用传递clientKey，但是如果在多个地方同时使用连续定位，每个地方需传递不同的clientKey，
销毁定位时需传递需要销毁的定位clientKey。
##### (4).后台保活定位：注意，一个程序仅允许一个保活定位进程，因为在安卓原生端只生成一个保活服务。一般程序也只需要在采集轨迹时才需要保活服务。

   ```dart
   //TODO:开启后台连续定位
   XbrLocation.instance().startBackgroundLocation(
        callback: (LocationInfo? location){
          //location 对IOS和Android数据 已兼容处理
        },
   );
   
   //业务结束时销毁定位
   XbrLocation.instance().destroyBackgroundLocation();
   
   ```


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
