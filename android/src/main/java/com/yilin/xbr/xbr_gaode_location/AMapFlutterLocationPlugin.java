package com.yilin.xbr.xbr_gaode_location;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.google.gson.reflect.TypeToken;
import com.yilin.xbr.xbr_gaode_location.location_service.LocationService;
import com.yilin.xbr.xbr_gaode_location.location_service.LocationStatusManager;
import com.yilin.xbr.xbr_gaode_location.location_service.utils.GsonUtil;
import com.yilin.xbr.xbr_gaode_location.location_service.utils.Utils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


/**
 * 高德地图定位sdkFlutterPlugin
 */
public class AMapFlutterLocationPlugin implements FlutterPlugin, MethodChannel.MethodCallHandler, ActivityAware, EventChannel.StreamHandler {
    private static final String CHANNEL_METHOD_LOCATION = "amap_flutter_location";
    private static final String CHANNEL_STREAM_LOCATION = "amap_flutter_location_stream";
    public static final String RECEIVER_ACTION = "location_in_background";
    public static final String xbr_gaode_background_location_key = "xbr_gaode_background_location";

    public static EventChannel.EventSink mEventSink = null;

    private BroadcastReceiver locationChangeBroadcastReceiver;
    private Activity activity = null;
    private Map<String, AMapLocationService> locationClientMap = new ConcurrentHashMap<>(8);
    private final AMapLocationClientOption backgroundLocationOption = new AMapLocationClientOption();

    @Override
    public void onMethodCall(MethodCall call, @NonNull MethodChannel.Result result) {
        String pluginKey = null;
        if (call.hasArgument("pluginKey")) pluginKey = call.argument("pluginKey");
        switch (call.method) {
            case "updatePrivacyStatement":
                Boolean hasContains = null, hasShow = null, hasAgree = null;
                if (call.hasArgument("hasContains")) hasContains = call.argument("hasContains");
                if (call.hasArgument("hasShow")) hasShow = call.argument("hasShow");
                if (call.hasArgument("hasAgree")) hasAgree = call.argument("hasAgree");
                updatePrivacyStatement(hasContains, hasShow, hasAgree);
                break;
            case "setApiKey":
                String androidKey = null;
                if (call.hasArgument("android")) androidKey = call.argument("android");
                setApiKey(androidKey);
                break;
            case "setLocationOption":
                Integer locationInterval = null, geoLanguage = null, locationMode = null;
                Boolean sensorEnable = null, needAddress = null, onceLocation = null;
                if (call.hasArgument("locationInterval")) locationInterval = call.argument("locationInterval");
                if (call.hasArgument("geoLanguage")) geoLanguage = call.argument("geoLanguage");
                if (call.hasArgument("locationMode")) locationMode = call.argument("locationMode");
                if (call.hasArgument("sensorEnable")) sensorEnable = call.argument("sensorEnable");
                if (call.hasArgument("needAddress")) needAddress = call.argument("needAddress");
                if (call.hasArgument("onceLocation")) onceLocation = call.argument("onceLocation");
                setLocationOption(pluginKey,locationInterval, sensorEnable, needAddress, geoLanguage, onceLocation, locationMode);
                break;
            case "startLocation":
                startLocation(pluginKey);
                break;
            case "stopLocation":
                stopLocation( pluginKey);
                break;
            case "destroy":
                destroy(pluginKey);
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    @Override
    public void onListen(Object o, EventChannel.EventSink eventSink) {
        mEventSink = eventSink;
    }

    @Override
    public void onCancel(Object o) {
        for (Map.Entry<String, AMapLocationService> entry : locationClientMap.entrySet()) {
            entry.getValue().stopLocation();
        }
    }

    /**
     * 设置apikey
     */
    private void setApiKey(String androidKey) {
        if (null != androidKey) {
            AMapLocationClient.setApiKey(androidKey);
        }
    }

    /**
     * 注册广播 new
     */
    protected void registerReceiver() {
        if (locationChangeBroadcastReceiver == null) {
            locationChangeBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(RECEIVER_ACTION)) {
                        String json = intent.getStringExtra("result");
                        Map<String, Object> result = GsonUtil.fromJson(json,new TypeToken<Map<String, Object>>(){});
                        result.put("pluginKey", xbr_gaode_background_location_key);
                        mEventSink.success(result);
                    }
                }
            };
        }
        /// 注册广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVER_ACTION);
        activity.registerReceiver(locationChangeBroadcastReceiver, intentFilter);
    }

    /**
     * 取消注册广播 new
     */
    protected void unRegisterReceiver() {
        if (locationChangeBroadcastReceiver != null) {
            activity.unregisterReceiver(locationChangeBroadcastReceiver);
            locationChangeBroadcastReceiver = null;
        }
    }

    /**
     * 启动后台定位服务 new
     */
    public void startLocationService() {
        Intent intent = new Intent(activity, LocationService.class);
        Bundle bundle = new Bundle();
        bundle.putLong("locationInterval", backgroundLocationOption.getInterval());
        bundle.putBoolean("sensorEnable", backgroundLocationOption.isSensorEnable());//传感器
        bundle.putBoolean("needAddress", backgroundLocationOption.isNeedAddress());//需求地址
        bundle.putInt("geoLanguage", backgroundLocationOption.getGeoLanguage().ordinal());//
        bundle.putBoolean("onceLocation", false);//后台定位不可能使用
        bundle.putInt("locationMode", backgroundLocationOption.getLocationMode().ordinal());//
        intent.putExtras(bundle);
        activity.getApplicationContext().startService(intent);
        LocationStatusManager.getInstance().resetToInit(activity.getApplicationContext());
    }

    /**
     * 关闭后台定位服务 new
     */
    public void stopLocationService() {
        activity.sendBroadcast(Utils.getCloseBrodecastIntent());
        LocationStatusManager.getInstance().resetToInit(activity.getApplicationContext());
    }

    /**
     * 设置定位参数
     */
    private void setLocationOption(String pluginKey,Integer locationInterval, Boolean sensorEnable, Boolean needAddress,
                                   Integer geoLanguage, Boolean onceLocation, Integer locationMode) {
        if (xbr_gaode_background_location_key.equals(pluginKey)) {
            if (null != locationInterval) backgroundLocationOption.setInterval(locationInterval.longValue());
            if (null != sensorEnable) backgroundLocationOption.setSensorEnable(sensorEnable);
            if (null != needAddress) backgroundLocationOption.setNeedAddress(needAddress);
            if (null != geoLanguage) backgroundLocationOption.setGeoLanguage(AMapLocationClientOption.GeoLanguage.values()[geoLanguage]);
            if (null != onceLocation) backgroundLocationOption.setOnceLocation(onceLocation);
            if (null != locationMode) backgroundLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.values()[locationMode]);
            //注册广播
            registerReceiver();
            return;
        }
        AMapLocationService locationClientImp = getLocationClientImp(pluginKey);
        if (null != locationClientImp) {
            locationClientImp.setLocationOption(locationInterval, sensorEnable, needAddress, geoLanguage, onceLocation, locationMode);
        }
    }

    /**
     * 开始定位
     */
    private void startLocation(String pluginKey) {
        if (xbr_gaode_background_location_key.equals(pluginKey)) {
            startLocationService();
            return;
        }
        AMapLocationService locationClientImp = getLocationClientImp(pluginKey);
        if (null != locationClientImp) {
            locationClientImp.startLocation();
        }
    }

    /**
     * 停止定位
     */
    private void stopLocation(String pluginKey) {
        if (xbr_gaode_background_location_key.equals(pluginKey)) {
            stopLocationService();
            return;
        }
        AMapLocationService locationClientImp = getLocationClientImp(pluginKey);
        if (null != locationClientImp) {
            locationClientImp.stopLocation();
        }
    }

    /**
     * 销毁
     */
    private void destroy(String pluginKey) {
        if (xbr_gaode_background_location_key.equals(pluginKey)) {
            unRegisterReceiver();
            return;
        }
        AMapLocationService locationClientImp = getLocationClientImp(pluginKey);
        if (null != locationClientImp) {
            locationClientImp.destroy();
            locationClientMap.remove(pluginKey);
        }
    }

    /**
     * 隐私协议
     */
    private void updatePrivacyStatement(Boolean hasContains, Boolean hasShow, Boolean hasAgree) {
        Class<AMapLocationClient> locationClazz = AMapLocationClient.class;
        try {
            if (hasContains!=null && hasShow!=null){
                Method showMethod = locationClazz.getMethod("updatePrivacyShow", Context.class, boolean.class, boolean.class);
                showMethod.invoke(null, activity.getApplicationContext(), hasContains, hasShow);
            }
            if (hasAgree!=null){
                Method agreeMethod = locationClazz.getMethod("updatePrivacyAgree", Context.class, boolean.class);
                agreeMethod.invoke(null, activity.getApplicationContext(), hasAgree);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        //方法调用通道
        final MethodChannel channel = new MethodChannel(binding.getBinaryMessenger(), CHANNEL_METHOD_LOCATION);
        channel.setMethodCallHandler(this);
        //回调监听通道
        final EventChannel eventChannel = new EventChannel(binding.getBinaryMessenger(), CHANNEL_STREAM_LOCATION);
        eventChannel.setStreamHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        for (Map.Entry<String, AMapLocationService> entry : locationClientMap.entrySet()) {
            entry.getValue().destroy();
        }
    }

    private AMapLocationService getLocationClientImp(String pluginKey) {
        if (null == locationClientMap) locationClientMap = new ConcurrentHashMap<>(8);
        if (TextUtils.isEmpty(pluginKey)) return null;
        if (!locationClientMap.containsKey(pluginKey)) {
            AMapLocationService locationClientImp = new AMapLocationService(activity.getApplicationContext(), pluginKey, mEventSink);
            locationClientMap.put(pluginKey, locationClientImp);
        }
        return locationClientMap.get(pluginKey);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }
}
