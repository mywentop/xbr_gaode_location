//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<xbr_gaode_location/AMapFlutterLocationPlugin.h>)
#import <xbr_gaode_location/AMapFlutterLocationPlugin.h>
#else
@import xbr_gaode_location;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [AMapFlutterLocationPlugin registerWithRegistrar:[registry registrarForPlugin:@"AMapFlutterLocationPlugin"]];
}

@end
