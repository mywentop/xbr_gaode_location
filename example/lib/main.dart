import 'package:flutter/material.dart';
import 'package:xbr_gaode_location/core/location.dart';
import 'package:xbr_gaode_location/xbr_gaode_location.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  int count = 0;
  LocationInfo? locationInfo;
  @override
  void initState() {
    super.initState();
    init();
    startOnceLocation();

    startLocation();

  }

  void init(){
    //初始化 建议在App启动时完成
    XbrGaodeLocation.initKey( androidKey:"268c81e5b00bce89e46992f22dc7e99b", iosKey:"");
    //注意这里没有做任何权限，直接启动不会成功
    //显示用户协议 建议在嵌软件使用协议 用户查看协议时调用
    XbrGaodeLocation.updatePrivacy( hasContains:true, hasShow:true,hasAgree: true);
  }

  void startLocation(){
    XbrGaodeLocation.instance().startTimeLocation(interval: 16000,backgroundService: true,callback: (LocationInfo? locationInfo){
      setState(() {
        count++;
        this.locationInfo = locationInfo;
      });
      print("哈哈：定位回调成功：${count}次；${DateTime.now()}");
      print(locationInfo?.locationTime??"-----------------------------------------------------");
      //连续定位根据配置时间间隔返回
      //建议在地图控件中测试 dev搜索 xbr_gaode_amap
    });
  }

  void startOnceLocation(){
    XbrGaodeLocation.instance().execOnceLocation(callback: (LocationInfo? locationInfo){
      //单次定位返回一次
      //建议在地图控件中测试 dev搜索 xbr_gaode_amap
    });
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('正在后台定位'),
        ),
        body: Center(
          child: Text('回调成功：$count次\n${locationInfo?.toJson()}'),
        ),
      ),
    );
  }
}
