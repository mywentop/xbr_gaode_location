import 'core/amap_flutter_location.dart';
import 'core/amap_location_option.dart';
import 'core/location.dart';

typedef LocationCallback = Function(LocationInfo locationInfo);

enum PsMode { high, middle, low }

class XbrGaodeLocation {
  ///工厂对象
  LocationInfo? currentLocation;
  String? fullAccuracyPurposeKey;

  XbrGaodeLocation({this.fullAccuracyPurposeKey});

  static XbrGaodeLocation? _instance;

  static XbrGaodeLocation instance() {
    _instance ??= XbrGaodeLocation();
    return _instance!;
  }

  Map<String,AMapFlutterLocation> clientMap = <String,AMapFlutterLocation>{};

  ///开始定位
  //优化 多线程回调 clientName 多个地方同时使用后台定位，这里可以同时返回多个回调通道
  void startTimeLocation(
      {String clientKey = "singleTimeLocation",
      int interval = 2000,
      double distance = -1,
      bool backgroundService = false, //新增，后台定位保活机制，一个运行终端只允许启动一个后台定位保活，一般用来后台采集轨迹，息屏也可以采集定位
      AMapLocationMode locationMode = AMapLocationMode.Hight_Accuracy,
      DesiredAccuracy desiredAccuracy = DesiredAccuracy.Best,
      required LocationCallback callback}) {

    //注意：定位已经在启动，不摧毁不要反复执行
    if(!clientMap.containsKey(clientKey)){
      AMapFlutterLocation? location = AMapFlutterLocation(backgroundService: backgroundService);
      //监听定位返回
      location.onLocationChanged().listen((Map<String, Object> result) {
        currentLocation = LocationInfo.fromJson(result);
        callback(currentLocation!);
      });
      clientMap[clientKey]= location;
    }

    AMapLocationOption locationOption = AMapLocationOption(
      onceLocation: false,
      locationInterval: interval,
      distanceFilter: distance,
      locationMode: locationMode,
      desiredAccuracy: desiredAccuracy,
      sensorEnable: true,
    );
    //适配ios14及以上 精准定位权限
    locationOption.fullAccuracyPurposeKey = fullAccuracyPurposeKey ?? "purposeKey";
    clientMap[clientKey]?.setLocationOption(locationOption);
    clientMap[clientKey]?.startLocation();
  }

  ///优化 多线程
  void destroyLocation({required String clientKey}) {
    //地图上开启定位（持续定位）必须跟随销毁，否则UI显示时会报错
    if (clientMap.containsKey(clientKey)) {
      clientMap[clientKey]?.stopLocation();
      clientMap[clientKey]?.destroy();
      currentLocation = null;
      clientMap.remove(clientKey);
    }
  }

  ///获取一次定位
  void execOnceLocation({
    String clientKey = "onceLocation",
    AMapLocationMode locationMode = AMapLocationMode.Hight_Accuracy,
    DesiredAccuracy desiredAccuracy = DesiredAccuracy.Best,
    required LocationCallback callback,
  }) {
    //已在后台定位中，直接返回定位
    if (currentLocation != null) {
      callback(currentLocation!);
      return;
    }
    if(!clientMap.containsKey(clientKey)){
      AMapFlutterLocation? location = AMapFlutterLocation();
      //监听定位返回
      location.onLocationChanged().listen((Map<String, Object> result) {
        LocationInfo location = LocationInfo.fromJson(result);
        callback(location);
        destroyLocation(clientKey: clientKey);
      });
      clientMap[clientKey]= location;
    }
    AMapLocationOption option = AMapLocationOption(
      onceLocation: true,
      locationMode: locationMode,
      desiredAccuracy: desiredAccuracy,
    );
    //适配ios14及以上 精准定位权限
    option.fullAccuracyPurposeKey = fullAccuracyPurposeKey ?? "purposeKey";
    clientMap[clientKey]?.setLocationOption(option);
    clientMap[clientKey]?.startLocation();
  }

  static void initKey({required String androidKey, required String iosKey}) {
    AMapFlutterLocation.setApiKey(androidKey, iosKey);
  }

  static void updatePrivacy({required bool hasContains, required bool hasShow, required bool hasAgree}) {
    AMapFlutterLocation.updatePrivacyShow(hasContains, hasShow);
    AMapFlutterLocation.updatePrivacyAgree(hasAgree);
  }
}
